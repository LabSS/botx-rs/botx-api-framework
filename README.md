# Botx-api-framework

## Описание
Фреймворк для разработки ботов под eXpress

## Использование
Добавьте в Cargo.toml
```toml
botx-api-framework = { version = "*", features = ["anthill-di"] }
```

Доступны следующие features:
`actix-web` - включение интеграции с actix-web (default)

Фреймворк не привязан к конкретному web фреймворку, но имеет готовую интеграцию с actix-web

Минимально для работы фреймворка нужно создать объект BotXApiFrameworkContext. Есть несколько способов:
* Через конструктор new - автоматически создастся контекст зависимостей и возьмутся настройки по умолчанию
* Через конструктор from_di_container - контекст зависимостей берется из аргументов, настройки берутся из зависимостей а при отсутствии берутся дефолтные
* Через регистрацию в anthill_di - контекстом зависимостей берется текущий контекст, настройки берутся из зависимостей а при отсутствии берутся дефолтные

```rs
// Пример создания контекста фреймворка из контекста anthill_di

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    let ioc_ctx = DependencyContext::new_root();

    ioc_ctx.register_botx_api_framework_context().await?;
    // Если не использовать метод расширения
    // ioc_ctx.register_type::<BotXApiFrameworkContext>(LifeCycle::Transient).await?;

    let context = ioc_ctx.resolve::<BotXApiFrameworkContext>().await?;
} 
```

После создания объекта фреймворка можно обрабатывать команды методами:
* ```process_command``` - обработка событий бота
* ```process_notification_result``` - обработка асинхронного результата
* ```process_status_result``` - обработка состояния бота

Интеграция с web actix добавляет методы расширения для actix_web::app::App:
* ```add_botx_api_handlers``` - добавляет обработку api (/command, /notification/callback, /status)
* ```add_smartapp_static_files``` - добавляет обработку запроса статики по api /smartapp_files/static/{path*}

Зарегистрированные api при запросе достают контекст фреймворка из зависимостей и передают ему управление

Для добавления обработчиков необходимо регистрировать соответствующий trait и зарегистрировать в контексте зависимостей    
Для удобства регистрации обработчиков добавлены методы расширения

Сопоставление trait-ов и методов расширения
|Описание|Trait для обработки события|Метод расширения|
|---|---|---|
|регистрирует регулярное выражения определяющее относится ли сообщение к командам, или простое сообщение|-|`register_command_detection_regex`|
|регистрирует обработчик команд и регулярное выражение для сопоставления сообщения с обработчиком|`ICommandHandler`|`register_command_handler`|
|регистрирует запасной обработчик команд|`ICommandHandler`|`register_default_command_handler`|
|регистрирует обработчик сообщений|`IMessageHandler`|`register_message_handler`|
|регистрирует обработчик кнопки|`IButtonHandler`|`register_button_handler`|
|регистрирует запасной обработчик кнопок|`IButtonHandler<TData = serde_json::Value, TMetaData = serde_json::Value>`|`register_default_button_handler`|
|регистрирует обработчик внутренней нотификации бота|`IInternalBotNotificationHandler`|`register_internal_notification_handler`|
|регистрирует запасной обработчик внутренней нотификации бота|`IInternalBotNotificationHandler<TData = serde_json::Value, TOptions = serde_json::Value>`|`register_default_internal_notification_handler`|
|регистрирует обработчик событий smartapp|`ISmartappEventHandler`|`register_smartapp_event_handler`|
|регистрирует запасной обработчик событий smartapp|`ISmartappEventHandler<TData = serde_json::Value, TOptions = serde_json::Value>`|`register_default_smartapp_event_handler`|
|регистрирует обработчик событий добавления в чат|`IAddedToChatHandler`|`register_add_to_chat_handler`|
|регистрирует обработчик событий выхода из чата|`ILeftFromChatHandler`|`register_left_from_chat_handler`|
|регистрирует обработчик события редактирования сообщения пользователем|`IEventEditHandler`|`register_event_edit_handler`|
|регистрирует обработчик событий удаления из чата|`IDeletedFromChatHandler`|`register_delete_from_chat_handler`|
|регистрирует обработчик событий создания чата|`IChatCreatedHandler`|`register_chat_created_handler`|
|регистрирует обработчик события удаления чата пользователем|`IChatDeletedByUserHandler`|`register_chat_deleted_by_user_handler`|
|регистрирует обработчик событий входа в cts|`ICtsLoginHandler`|`register_cts_login_handler`|
|регистрирует обработчик событий выхода из cts|`ICtsLogoutHandler`|`register_cts_logout_handler`|
|регистрирует обработчик асинхронного результата события|`INotificationResultHandler`|`register_notification_callback_handler`|
|регистрирует обработчик запроса состояния бота|`IStatusHandler`|`register_status_handler`|

Для обработки кнопок TData и TMetadata обработчика должны примерять макрос `button_data`:
```rs
#[button_data]
pub struct ButtonData {
    pub message: String,
}
```

Макрос button_data добавляет в Data и MetaData поле type_id в котором должен храниться тип объекта. По type_id происходит сопоставление кнопок и обработчиков. Объект в поле type_id имеет реализацию Default, по этому создание объектов data и metadata выполняется способом ниже:

```rs
let data = ButtonData { type_id: Default::default(), message: "test message".to_string() }
```

Сопоставление команд идет по указанному регулярному выражению    
Сопоставление событий smartapp и внутренней бот нотификации происходит при помощи десериализации - если сообщение можно разобрать в объекты указанные в обработчике, значит этому обработчику будет передано событие 

Для работы с api eXpress этот crate реэкспортирует crate botx-api. Используйте метод расширение для регистрации контекста api в  контексте зависимостей, после чего его можно будет получить в каждом обработчике и выполнять запросы в eXpress

Базовая настройка приложения может выглядеть следующим образом
```rs
use actix_web::{
    middleware::Logger,
    web::{self, Data},
    App, HttpServer,
};

use anthill_di::DependencyContext;
use botx_api::extensions::botx_api_context::IBotXApiContextExt;
use botx_api_framework::extensions::{anthill_di::IAnthillDiExt, actix::IActixHandlersExt};

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    let ioc_ctx = DependencyContext::new_root();

    ioc_ctx.register_botx_api_context().await?;
    ioc_ctx.register_botx_api_framework_context().await?;

    HttpServer::new(move || {
        App::new()
            .wrap(Logger::default())
            .app_data(Data::new(ioc_ctx.clone()))
            .add_botx_api_handlers()
    })
    .bind(("127.0.0.1", "7856"))?
    .run()
    .await?;

    Ok(())
}

```

## Вклад
Откройте issue с вопросом или предложением

## Лицензия
MIT, можно использовать в любых целях

## Статус проекта
|Состояние|Фича|Доп. описание|
|---|---|---|
|✅|Интеграция с actix-web|Веб сервер на основе actix-web|
|✅|Обработчик сообщения|Регистрация своего обработчика сообщений пользователей|
|✅|Определение своего детектора команд|Возможность задать свое регулярное выражения определения относится ли сообщение к командам|
|✅|Обработчик команды|Возможность создавать обработчики команд и назначать им регулярные выражения для сопоставления|
|✅|Обработчик не обработанных команды|Возможность создавать обработчик для команд для которых не найден обработчик|
|✅|Обработчик кнопок|Возможность создавать обработчики кнопок и сопоставлять кнопки сообщений с обработчиками через data и metadata которые может обработать обработчик|
|✅|Обработчик не обработанных кнопок|Возможность создавать обработчик кнопок для кнопок для которых не найден обработчик|
|✅|Обработчик события добавления в чат|Возможность создавать обработчики события добавления в чат|
|✅|Обработчик события выхода из чата|Возможность создавать обработчики события выхода из чата|
|✅|Обработчик события редактирования сообщения пользователем|Возможность создавать обработчики события редактирования сообщения пользователем|
|✅|Обработчик события удаления из чата|Возможность создавать обработчики события удаления из чата|
|✅|Обработчик события удаления чата пользователем|Возможность создавать обработчики события удаления чата пользователем|
|✅|Обработчик события создания чата|Возможность создавать обработчики события создания чата|
|✅|Обработчик события выхода с cts|Возможность создавать обработчики события выхода из cts|
|✅|Обработчик события входа в cts|Возможность создавать обработчики события входа в cts|
|✅|Обработчик события нотификации бота|Возможность создавать обработчики событий от других ботов|
|✅|Обработчик события не обработанных нотификаций бота|Возможность создавать обработчики событий от других ботов для которых не найден обработчик|
|✅|Обработчик событий smartapp|Возможность создавать обработчики событий от smartapp и сопоставлять с обработчиками через data и metadata которые может обработать обработчик|
|✅|Обработчик не обработанных событий smartapp|Возможность создавать обработчики событий от smartapp для которых не были найдены обработчики|
|✅|Обработчик событий асинхронного результатов|Возможность создавать общий обработчик события асинхронного результата|
|✅|Обработчик событий status|Возможность создавать обработчик запроса состояния бота|
|✅|Написать тесты для всех обработчиков||
