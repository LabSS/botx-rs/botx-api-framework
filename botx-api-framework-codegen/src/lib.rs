use proc_macro::TokenStream;
use quote::format_ident;
use syn::{*, parse::Parser};

#[proc_macro_attribute]
pub fn button_data(args: TokenStream, input: TokenStream) -> TokenStream {
    let mut item_struct = parse_macro_input!(input as ItemStruct);
    let _ = parse_macro_input!(args as parse::Nothing);
    
    let name = item_struct.ident.clone();

    let method_name = format_ident!("get_type_id_{}", name.to_string());
    let method_name_str = method_name.to_string();

    let custom_type_id_name = format_ident!("{}TypeId", name.to_string());

    if let syn::Fields::Named(ref mut fields) = item_struct.fields {
        let mut field = syn::Field::parse_named
            .parse2(quote::quote! { pub type_id: #custom_type_id_name })
            .unwrap();

        fields.named.push(field);
    }

    return quote::quote! {
        #[derive(serde::Serialize, serde::Deserialize)]
        #item_struct

        #[derive(serde::Serialize, serde::Deserialize)]
        pub struct #custom_type_id_name (pub String);

        impl Default for #custom_type_id_name {
            fn default() -> Self {
                Self(std::any::type_name::<#name>().to_string())
            }
        }

        impl Into<serde_json::Value> for #name {
            fn into(self) -> serde_json::Value {
                serde_json::to_value(self).unwrap()
            }
        }
    }
    .into();
}