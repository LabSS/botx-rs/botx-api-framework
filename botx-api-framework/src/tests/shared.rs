use std::collections::HashMap;

use anthill_di_derive::*;
use botx_api::bot::models::*;
use botx_api_framework_codegen::button_data;
use serde::{Deserialize, Serialize};
use uuid::Uuid;

use crate::{
    contexts::*,
    handlers::{
        command::*,
        message::*,
        button::*,
        chat_created::*,
        added_to_chat::*,
        deleted_from_chat::*,
        left_from_chat::*, cts_login::ICtsLoginHandler, cts_logout::ICtsLogoutHandler, internal_bot_notification::IInternalBotNotificationHandler, smartapp_event::ISmartappEventHandler, chat_deleted_by_user::IChatDeletedByUserHandler, edit_event::IEventEditHandler
    },
    results::*,
};

#[derive(constructor)]
pub struct StartCommandHandler {
    #[custom_resolve(value = "\"start response\".to_string()")]
    pub response: String,
}

#[async_trait_with_sync::async_trait]
impl ICommandHandler for StartCommandHandler {
    async fn handle(&mut self, command: String, _: RequestContext) -> CommandResult {
        CommandResult::Ok(CommandOk::new(format!("{} - {}", self.response, command)))
    }
}

#[derive(constructor)]
pub struct DoSomethingCommandHandler {
    #[custom_resolve(value = "\"do something response\".to_string()")]
    pub response: String,
}

#[async_trait_with_sync::async_trait]
impl ICommandHandler for DoSomethingCommandHandler {
    async fn handle(&mut self, command: String, _: RequestContext) -> CommandResult {
        CommandResult::Ok(CommandOk::new(format!("{} - {}", self.response, command)))
    }
}

#[derive(constructor)]
pub struct DefaultCommandHandler {
    #[custom_resolve(value = "\"default response\".to_string()")]
    pub response: String,
}

#[async_trait_with_sync::async_trait]
impl ICommandHandler for DefaultCommandHandler {
    async fn handle(&mut self, command: String, _: RequestContext) -> CommandResult {
        CommandResult::Ok(CommandOk::new(format!("{} - {}", self.response, command)))
    }
}

#[derive(constructor)]
pub struct MessageHandler {
    #[custom_resolve(value = "\"message processed\".to_string()")]
    pub response: String,
}

#[async_trait_with_sync::async_trait]
impl IMessageHandler for MessageHandler {
    async fn handle(&mut self, message: String, _request_context: RequestContext) -> CommandResult {
        CommandResult::Ok(CommandOk::new(format!("{} - {}", self.response, message)))
    }
}

#[derive(constructor)]
pub struct ButtonHandler {
    #[custom_resolve(value = "\"button processed\".to_string()")]
    pub response: String,
}

#[button_data]
pub struct ButtonData {
    pub text_data: String,
}

#[button_data]
pub struct EmptyButtonData {}

#[button_data]
pub struct ButtonMetaData {
    pub text_metadata: String,
}

#[async_trait_with_sync::async_trait]
impl IButtonHandler for ButtonHandler {
    type TData = ButtonData;
    type TMetaData = ButtonMetaData;

    async fn handle(
        &mut self,
        button_text: String,
        data: Self::TData,
        metadata: Self::TMetaData,
        _request_context: RequestContext,
    ) -> CommandResult {
        CommandResult::Ok(CommandOk::new(format!(
            "{} - {} - {} - {}",
            self.response, button_text, data.text_data, metadata.text_metadata
        )))
    }
}

#[derive(constructor)]
pub struct DefaultButtonHandler {
    #[custom_resolve(value = "\"default button processed\".to_string()")]
    pub response: String,
}

#[async_trait_with_sync::async_trait]
impl IButtonHandler for DefaultButtonHandler {
    type TData = serde_json::Value;
    type TMetaData = serde_json::Value;

    async fn handle(
        &mut self,
        button_text: String,
        _data: Self::TData,
        _metadata: Self::TMetaData,
        _request_context: RequestContext,
    ) -> CommandResult {
        CommandResult::Ok(CommandOk::new(format!(
            "{} - {}",
            self.response, button_text
        )))
    }
}

#[derive(constructor)]
pub struct ChatCreatedHandler {
    #[custom_resolve(value = "\"event precessed\".to_string()")]
    pub response: String,
}

#[async_trait_with_sync::async_trait]
impl IChatCreatedHandler for ChatCreatedHandler {
    async fn handle(
        &mut self,
        event: ChatCreatedCommand<serde_json::Value>,
        _request_context: RequestContext
    ) -> CommandResult {
        CommandResult::Ok(CommandOk::new(format!(
            "{}. Chat name: {}",
            self.response, event.data.name
        )))
    }
}

#[derive(constructor)]
pub struct ChatDeletedByUserHandler {
    #[custom_resolve(value = "\"event precessed\".to_string()")]
    pub response: String,
}

#[async_trait_with_sync::async_trait]
impl IChatDeletedByUserHandler for ChatDeletedByUserHandler {
    async fn handle(
        &mut self,
        event: ChatDeletedByUserCommand<serde_json::Value>,
        _request_context: RequestContext
    ) -> CommandResult {
        CommandResult::Ok(CommandOk::new(format!(
            "{}. Chat name: {} {}",
            self.response, event.data.group_chat_id, event.data.user_huid
        )))
    }
}

#[derive(constructor)]
pub struct AddedToChatHandler {
    #[custom_resolve(value = "\"event precessed\".to_string()")]
    pub response: String,
}

#[async_trait_with_sync::async_trait]
impl IAddedToChatHandler for AddedToChatHandler {
    async fn handle(
        &mut self,
        event: AddedToChatCommand<serde_json::Value>,
        _request_context: RequestContext
    ) -> CommandResult {
        CommandResult::Ok(CommandOk::new(format!(
            "{}. Members count: {}",
            self.response, event.data.added_members.len()
        )))
    }
}

#[derive(constructor)]
pub struct DeletedFromChatHandler {
    #[custom_resolve(value = "\"event precessed\".to_string()")]
    pub response: String,
}

#[async_trait_with_sync::async_trait]
impl IDeletedFromChatHandler for DeletedFromChatHandler {
    async fn handle(
        &mut self,
        event: DeletedFromChatCommand<serde_json::Value>,
        _request_context: RequestContext
    ) -> CommandResult {
        CommandResult::Ok(CommandOk::new(format!(
            "{}. Members count: {}",
            self.response, event.data.deleted_members.len()
        )))
    }
}

#[derive(constructor)]
pub struct LeftFromChatHandler {
    #[custom_resolve(value = "\"event precessed\".to_string()")]
    pub response: String,
}

#[async_trait_with_sync::async_trait]
impl ILeftFromChatHandler for LeftFromChatHandler {
    async fn handle(
        &mut self,
        event: LeftFromChatCommand<serde_json::Value>,
        _request_context: RequestContext
    ) -> CommandResult {
        CommandResult::Ok(CommandOk::new(format!(
            "{}. Members count: {}",
            self.response, event.data.left_members.len()
        )))
    }
}

#[derive(constructor)]
pub struct EventEditHandler {
    #[custom_resolve(value = "\"event precessed\".to_string()")]
    pub response: String,
}

#[async_trait_with_sync::async_trait]
impl IEventEditHandler for EventEditHandler {
    async fn handle(
        &mut self,
        event: EventEditCommand<serde_json::Value>,
        _request_context: RequestContext
    ) -> CommandResult {
        CommandResult::Ok(CommandOk::new(format!(
            "{}. {}",
            self.response, event.data.body
        )))
    }
}

#[derive(constructor)]
pub struct CtsLoginHandler {
    #[custom_resolve(value = "\"event precessed\".to_string()")]
    pub response: String,
}

#[async_trait_with_sync::async_trait]
impl ICtsLoginHandler for CtsLoginHandler {
    async fn handle(
        &mut self,
        event: CtsLoginCommand<serde_json::Value>,
        _request_context: RequestContext
    ) -> CommandResult {
        CommandResult::Ok(CommandOk::new(format!(
            "{} - {} - {}",
            self.response, event.data.cts_id, event.data.user_huid
        )))
    }
}

#[derive(constructor)]
pub struct CtsLogoutHandler {
    #[custom_resolve(value = "\"event precessed\".to_string()")]
    pub response: String,
}

#[async_trait_with_sync::async_trait]
impl ICtsLogoutHandler for CtsLogoutHandler {
    async fn handle(
        &mut self,
        event: CtsLogoutCommand<serde_json::Value>,
        _request_context: RequestContext
    ) -> CommandResult {
        CommandResult::Ok(CommandOk::new(format!(
            "{} - {} - {}",
            self.response, event.data.cts_id, event.data.user_huid
        )))
    }
}

#[derive(constructor)]
pub struct InternalBotNotificationHandlerFirst {
    #[custom_resolve(value = "\"event precessed 1\".to_string()")]
    pub response: String,
}

#[derive(Deserialize, Serialize)]
pub struct InternalBotNotificationHandlerFirstData {
    pub some_data_text: String,
}

#[derive(Deserialize, Serialize)]
pub struct InternalBotNotificationHandlerOptions {
    pub some_id: uuid::Uuid,
    pub some_text: String,
}

#[async_trait_with_sync::async_trait]
impl IInternalBotNotificationHandler for InternalBotNotificationHandlerFirst {
    type TData = InternalBotNotificationHandlerFirstData;
    type TOptions = InternalBotNotificationHandlerOptions;

    async fn handle(&mut self, data: Self::TData, options: Self::TOptions, _request_context: RequestContext) -> CommandResult {
        CommandResult::Ok(CommandOk::new(format!(
            "{} - {} - {} - {}",
            self.response, data.some_data_text, options.some_id, options.some_text
        )))
    }
}

#[derive(constructor)]
pub struct InternalBotNotificationHandlerSecond {
    #[custom_resolve(value = "\"event precessed 2\".to_string()")]
    pub response: String,
}

#[derive(Deserialize, Serialize)]
pub struct InternalBotNotificationHandlerSecondData {
    pub some_data_text_2: String,
}

#[async_trait_with_sync::async_trait]
impl IInternalBotNotificationHandler for InternalBotNotificationHandlerSecond {
    type TData = InternalBotNotificationHandlerSecondData;
    type TOptions = InternalBotNotificationHandlerOptions;

    async fn handle(&mut self, data: Self::TData, options: Self::TOptions, _request_context: RequestContext) -> CommandResult {
        CommandResult::Ok(CommandOk::new(format!(
            "{} - {} - {} - {}",
            self.response, data.some_data_text_2, options.some_id, options.some_text
        )))
    }
}

#[derive(constructor)]
pub struct InternalBotNotificationDefaultHandler {
    #[custom_resolve(value = "\"event precessed default\".to_string()")]
    pub response: String,
}

#[async_trait_with_sync::async_trait]
impl IInternalBotNotificationHandler for InternalBotNotificationDefaultHandler {
    type TData = serde_json::Value;
    type TOptions = serde_json::Value;

    async fn handle(&mut self, data: Self::TData, options: Self::TOptions, _request_context: RequestContext) -> CommandResult {
        CommandResult::Ok(CommandOk::new(format!(
            "{} - {:#?} - {:#?}",
            self.response, data, options
        )))
    }
}

#[derive(constructor)]
pub struct SmartappEventHandlerFirst {
    #[custom_resolve(value = "\"event precessed 1\".to_string()")]
    pub response: String,
}

#[derive(Deserialize, Serialize)]
pub struct SmartappEventHandlerFirstData {
    pub some_data_text: String,
}

#[derive(Deserialize, Serialize)]
pub struct SmartappEventHandlerOptions {
    pub some_id: uuid::Uuid,
    pub some_text: String,
}

#[async_trait_with_sync::async_trait]
impl ISmartappEventHandler for SmartappEventHandlerFirst {
    type TData = SmartappEventHandlerFirstData;
    type TOptions = SmartappEventHandlerOptions;

    async fn handle(&mut self, _request_ref: Uuid, _smartapp_id: Uuid, _smartapp_api_version: u16, data: Self::TData, options: Self::TOptions, _request_context: RequestContext) -> CommandResult {
        CommandResult::Ok(CommandOk::new(format!(
            "{} - {} - {} - {}",
            self.response, data.some_data_text, options.some_id, options.some_text
        )))
    }
}

#[derive(constructor)]
pub struct SmartappEventHandlerSecond {
    #[custom_resolve(value = "\"event precessed 2\".to_string()")]
    pub response: String,
}

#[derive(Deserialize, Serialize)]
pub enum Method {
    Get,
    Post,
}

#[derive(Deserialize, Serialize)]
pub struct SmartappEventHandlerSecondData {
    pub url: String,
    pub method: Method,
    pub headers: HashMap<String, String>,
    pub body: String,
}

#[async_trait_with_sync::async_trait]
impl ISmartappEventHandler for SmartappEventHandlerSecond {
    type TData = SmartappEventHandlerSecondData;
    type TOptions = SmartappEventHandlerOptions;

    async fn handle(&mut self, _request_ref: Uuid, _smartapp_id: Uuid, _smartapp_api_version: u16, data: Self::TData, options: Self::TOptions, _request_context: RequestContext) -> CommandResult {
        CommandResult::Ok(CommandOk::new(format!(
            "{} - {} - {} - {} - {}",
            self.response, data.url, data.body, options.some_id, options.some_text
        )))
    }
}

#[derive(constructor)]
pub struct SmartappEventDefaultHandler {
    #[custom_resolve(value = "\"event precessed default\".to_string()")]
    pub response: String,
}

#[async_trait_with_sync::async_trait]
impl ISmartappEventHandler for SmartappEventDefaultHandler {
    type TData = serde_json::Value;
    type TOptions = serde_json::Value;

    async fn handle(&mut self, _request_ref: Uuid, _smartapp_id: Uuid, _smartapp_api_version: u16, data: Self::TData, options: Self::TOptions, _request_context: RequestContext) -> CommandResult {
        CommandResult::Ok(CommandOk::new(format!(
            "{} - {:#?} - {:#?}",
            self.response, data, options
        )))
    }
}