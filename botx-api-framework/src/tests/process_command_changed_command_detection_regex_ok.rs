use std::str::FromStr;

use botx_api::bot::models::*;
use regex::Regex;
use serde_json::json;

use crate::{
    contexts::*,
    tests::shared::*,
    extensions::anthill_di::*
};

#[tokio::test]
async fn process_command_changed_command_detection_regex_ok() {
    let bot_context = BotXApiFrameworkContext::new();
    bot_context.di_container()
        .register_command_detection_regex(Regex::new("^#").unwrap())
        .await
        .unwrap();

    bot_context.di_container()
        .register_command_handler::<StartCommandHandler>(Regex::new("^#start").unwrap())
        .await
        .unwrap();

    bot_context.di_container()
        .register_command_handler::<DoSomethingCommandHandler>(Regex::new("^#do something").unwrap())
        .await
        .unwrap();

    let result = bot_context.process_command(CommandRequest::<serde_json::Value, serde_json::Value> {
            sync_id: uuid::Uuid::from_str("a465f0f3-1354-491c-8f11-f400164295cb").unwrap(),
            source_sync_id: None,
            command: Command::Message(MessageCommand {
                body: "#start какая то инфа".to_string(),
                data: json!({}),
                metadata: json!({}),
                command_type: CommandType::User,
            }),
            attachments: vec![],
            from: From {
                user_huid: None,
                group_chat_id: None,
                chat_type: None,
                ad_login: None,
                ad_domain: None,
                username: None,
                is_admin: None,
                is_creator: None,
                manufacturer: None,
                device: None,
                device_software: None,
                device_meta: None,
                platform: None,
                platform_package_id: None,
                app_version: None,
                locale: None,
                host: None,
            },
            async_files: vec![],
            bot_id: uuid::Uuid::from_str("a465f0f3-1354-491c-8f11-f400164295cb").unwrap(),
            proto_version: 4,
            entities: vec![],
        })
        .await;

    let result = result.unwrap();

    assert_eq!(
        result.result,
        format!("{} - {}", "start response", "#start какая то инфа").to_string()
    );

    let result = bot_context.process_command(CommandRequest::<serde_json::Value, serde_json::Value> {
            sync_id: uuid::Uuid::from_str("a465f0f3-1354-491c-8f11-f400164295cb").unwrap(),
            source_sync_id: None,
            command: Command::Message(MessageCommand {
                body: "#do something какая то инфа".to_string(),
                data: json!({}),
                metadata: json!({}),
                command_type: CommandType::User,
            }),
            attachments: vec![],
            from: From {
                user_huid: None,
                group_chat_id: None,
                chat_type: None,
                ad_login: None,
                ad_domain: None,
                username: None,
                is_admin: None,
                is_creator: None,
                manufacturer: None,
                device: None,
                device_software: None,
                device_meta: None,
                platform: None,
                platform_package_id: None,
                app_version: None,
                locale: None,
                host: None,
            },
            async_files: vec![],
            bot_id: uuid::Uuid::from_str("a465f0f3-1354-491c-8f11-f400164295cb").unwrap(),
            proto_version: 4,
            entities: vec![],
        })
        .await;

    let result = result.unwrap();

    assert_eq!(
        result.result,
        format!(
            "{} - {}",
            "do something response", "#do something какая то инфа"
        )
        .to_string()
    );
}
