use std::str::FromStr;

use anthill_di::*;
use botx_api::bot::models::*;
use serde_json::json;

use crate::{
    tests::shared::*,
    contexts::*,
    extensions::anthill_di::*
};

#[tokio::test]
async fn process_button_default_handler_when_invalid_format_ok() {
    let ioc_context = DependencyContext::new_root();
    ioc_context.register_instance(BotXApiFrameworkContextSettings {
            use_default_button_handler_if_format_or_setup_error: true,
        })
        .await
        .unwrap();

    let bot_context = BotXApiFrameworkContext::from_di_container(ioc_context).await;

    bot_context.di_container()
        .register_button_handler::<ButtonHandler>()
        .await
        .unwrap();

    bot_context.di_container()
        .register_default_button_handler::<DefaultButtonHandler>()
        .await
        .unwrap();

    let result = bot_context.process_command(CommandRequest::<serde_json::Value, serde_json::Value> {
            sync_id: uuid::Uuid::from_str("a465f0f3-1354-491c-8f11-f400164295cb").unwrap(),
            source_sync_id: Some(uuid::Uuid::from_str("a465f0f3-1354-491c-8f11-f400164295cb").unwrap()),
            command: Command::Message(MessageCommand {
                body: "какая то инфа".to_string(),
                data: serde_json::value::to_value( EmptyButtonData{ type_id: Default::default() }).unwrap(),
                metadata: json!({}),
                command_type: CommandType::User,
            }),
            attachments: vec![],
            from: From {
                user_huid: None,
                group_chat_id: None,
                chat_type: None,
                ad_login: None,
                ad_domain: None,
                username: None,
                is_admin: None,
                is_creator: None,
                manufacturer: None,
                device: None,
                device_software: None,
                device_meta: None,
                platform: None,
                platform_package_id: None,
                app_version: None,
                locale: None,
                host: None,
            },
            async_files: vec![],
            bot_id: uuid::Uuid::from_str("a465f0f3-1354-491c-8f11-f400164295cb").unwrap(),
            proto_version: 4,
            entities: vec![],
        }).await;

    let result = result.unwrap();

    assert_eq!(
        result.result,
        format!("{} - {}", "default button processed", "какая то инфа").to_string()
    );
}
