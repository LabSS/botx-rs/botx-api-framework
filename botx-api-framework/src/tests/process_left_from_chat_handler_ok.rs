use std::str::FromStr;

use botx_api::bot::models::*;
use serde_json::json;

use crate::{
    contexts::*,
    tests::shared::*, extensions::anthill_di::IAnthillDiExt
};

#[tokio::test]
async fn process_left_from_chat_handler_ok() {
    let bot_context = BotXApiFrameworkContext::new();

    bot_context.di_container()
        .register_left_from_chat_handler::<LeftFromChatHandler>()
        .await
        .unwrap();

    let result = bot_context.process_command(CommandRequest::<serde_json::Value, serde_json::Value> {
            sync_id: uuid::Uuid::from_str("a465f0f3-1354-491c-8f11-f400164295cb").unwrap(),
            source_sync_id: None,
            command: Command::LeftFromChat(LeftFromChatCommand {
                data: LeftFromChatCommandData { left_members: vec![
                    uuid::Uuid::from_str("a465f0f3-1354-491c-8f11-f400164295c1").unwrap(),
                    uuid::Uuid::from_str("a465f0f3-1354-491c-8f11-f400164295c2").unwrap()
                ] },
                metadata: json!({}),
                command_type: CommandType::System,
            }),
            attachments: vec![],
            from: From {
                user_huid: None,
                group_chat_id: None,
                chat_type: None,
                ad_login: None,
                ad_domain: None,
                username: None,
                is_admin: None,
                is_creator: None,
                manufacturer: None,
                device: None,
                device_software: None,
                device_meta: None,
                platform: None,
                platform_package_id: None,
                app_version: None,
                locale: None,
                host: None,
            },
            async_files: vec![],
            bot_id: uuid::Uuid::from_str("a465f0f3-1354-491c-8f11-f400164295cb").unwrap(),
            proto_version: 4,
            entities: vec![],
        })
        .await;

    let result = result.unwrap();

    assert_eq!(
        result.result,
        format!("{}. Members count: {}", "event precessed", 2).to_string()
    );
}
