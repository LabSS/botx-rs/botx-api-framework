use std::path::PathBuf;

use actix_web::{App, dev::{ServiceFactory, ServiceRequest}, Error};
use easy_ext::ext;

use crate::integration::actix::{
    command_handler::command,
    notification_result_handler::notification_callback,
    status_handler::status
};

#[cfg(feature = "actix-web")]
#[ext(IActixHandlersExt)]
#[async_trait_with_sync::async_trait]
pub impl<T> App<T>
where
    T: ServiceFactory<ServiceRequest, Config = (), Error = Error, InitError = ()>,
{
    fn add_botx_api_handlers(self) -> App<T> {
        self.service(command)
            .service(notification_callback)
            .service(status)
    }

    fn add_smartapp_static_files(self, static_files_path: impl Into<PathBuf>) -> App<T> {
        self.service(actix_files::Files::new("/smartapp_files/static", static_files_path).show_files_listing())
    }
}