

pub type CommandResult = Result<CommandOk, CommandError>;

#[derive(Debug)]
pub struct CommandOk {
    pub result: String,
}

impl CommandOk {
    pub fn new<TResult: Into<String>>(result: TResult) -> Self { Self { result: result.into() } }
}

impl Default for CommandOk {
    fn default() -> Self {
        Self { result: "accepted".to_string() }
    }
}

#[derive(Debug)]
pub struct CommandError {
    /// Причина ошибки
    pub reason: String,

    /// Сообщение которое будет показано пользователю (TODO: проверить что оно реально показывается)
    pub status_message: String,
}

impl CommandError {
    pub fn new(reason: String, status_message: String) -> Self { Self { reason, status_message } }
}

