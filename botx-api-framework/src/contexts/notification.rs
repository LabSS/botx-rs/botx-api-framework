use std::collections::HashMap;

use anthill_di_derive::constructor;
use async_oneshot::{oneshot, Sender, Receiver};
use uuid::Uuid;


#[derive(constructor)]
pub struct NotificationContext {
    #[custom_resolve(value = "Default::default()")]
    notification_waiters: HashMap<Uuid, Sender<()>>
}

impl NotificationContext {
    pub fn new() -> Self { Self { notification_waiters: Default::default() } }

    pub fn set_callback(&mut self, sync_id: Uuid) -> Receiver<()> {
        let (sender,receiver) = oneshot::<()>();
        self.notification_waiters.insert(sync_id, sender);
        receiver
    }

    pub fn event(&mut self, sync_id: Uuid) {
        self.notification_waiters.remove(&sync_id)
            .map(|mut x| { x.send(()); });
    }
}
