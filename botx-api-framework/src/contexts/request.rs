use botx_api::{
    bot::models::*, api::models::AsyncFile,
};
use uuid::Uuid;

#[derive(Debug)]
pub struct RequestContext {
    /// идентификатор сообщения в системе Express
    pub sync_id: Uuid,

    /// (Default: null) - идентификатор исходного сообщения (сообщения в котором находились элементы интерфейса) в системе Express
    pub source_sync_id: Option<Uuid>,

    /// вложения, переданные в сообщении<br/>
    /// Например: изображения, видео, файлы, ссылки, геолокации, контакты 
    pub attachments: Vec<Attachment>,

    pub from: From,

    /// метаданные файлов для отложенной обработки 
    pub async_files: Vec<AsyncFile>,

    /// идентификатор бота в системе Express
    pub bot_id: Uuid,

    /// версия протокола (BotX -> Bot) используемая при отправке команды
    pub proto_version: u16,

    /// особые сущности переданные в сообщение. Например: меншны, хэштеги, ссылки, форварды 
    pub entities: Vec<CommandEntities>,
}