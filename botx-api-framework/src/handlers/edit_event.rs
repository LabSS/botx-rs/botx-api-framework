use botx_api::bot::models::EventEditCommand;

use crate::{contexts::RequestContext, results::CommandResult};

#[async_trait_with_sync::async_trait]
pub trait IEventEditHandler: Send + Sync {
    async fn handle(&mut self, event: EventEditCommand<serde_json::Value>, request_context: RequestContext) -> CommandResult;
}
