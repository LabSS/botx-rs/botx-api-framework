use botx_api::bot::models::DeletedFromChatCommand;

use crate::{contexts::RequestContext, results::CommandResult};

#[async_trait_with_sync::async_trait]
pub trait IDeletedFromChatHandler: Send + Sync {
    async fn handle(&mut self, event: DeletedFromChatCommand<serde_json::Value>, request_context: RequestContext) -> CommandResult;
}