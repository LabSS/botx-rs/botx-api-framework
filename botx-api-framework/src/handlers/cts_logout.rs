use botx_api::bot::models::CtsLogoutCommand;

use crate::{contexts::RequestContext, results::CommandResult};

#[async_trait_with_sync::async_trait]
pub trait ICtsLogoutHandler: Send + Sync {
    async fn handle(&mut self, event: CtsLogoutCommand<serde_json::Value>, request_context: RequestContext) -> CommandResult;
}