use botx_api::bot::models::*;

#[async_trait_with_sync::async_trait]
pub trait IStatusHandler: Send + Sync {
    async fn handle(&mut self, result: StatusRequest) -> StatusResponse;
} 