use botx_api::bot::models::AddedToChatCommand;

use crate::{contexts::RequestContext, results::CommandResult};

#[async_trait_with_sync::async_trait]
pub trait IAddedToChatHandler: Send + Sync {
    async fn handle(&mut self, event: AddedToChatCommand<serde_json::Value>, request_context: RequestContext) -> CommandResult;
}