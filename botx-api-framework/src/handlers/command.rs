use std::{any::TypeId, ops::Deref, marker::PhantomData};

use regex::Regex;

use crate::{results::CommandResult, contexts::RequestContext};

pub struct DefaultCommandHandlerId(pub TypeId);

impl Deref for DefaultCommandHandlerId {
    type Target = TypeId;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

#[derive(Clone)]
pub struct CommandHandlerId(pub TypeId);

impl Deref for CommandHandlerId {
    type Target = TypeId;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

#[derive(Clone)]
pub struct CommandDetectionRegex(pub Regex);

impl Into<CommandDetectionRegex> for Regex {
    fn into(self) -> CommandDetectionRegex {
        CommandDetectionRegex(self)
    }
}

pub trait ICommandHandlerMatchingRule: Sync + Send {
    fn get_command_regex(&self) -> &CommandDetectionRegex;
    fn get_command_handler_id(&self) -> CommandHandlerId;
}

#[derive(Clone)]
pub struct CommandHandlerMatchingRule<TCommandHandler: ICommandHandler + 'static> {
    pub command_regex: CommandDetectionRegex,
    pub pd: PhantomData<TCommandHandler>,
}

impl<TCommandHandler: ICommandHandler + 'static> ICommandHandlerMatchingRule for CommandHandlerMatchingRule<TCommandHandler> {
    fn get_command_regex(&self) -> &CommandDetectionRegex {
        &self.command_regex
    }

    fn get_command_handler_id(&self) -> CommandHandlerId {
        CommandHandlerId(TypeId::of::<TCommandHandler>())
    }
}

impl Deref for CommandDetectionRegex {
    type Target = Regex;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

#[async_trait_with_sync::async_trait]
pub trait ICommandHandler: Send + Sync {
    async fn handle(&mut self, command: String, request_context: RequestContext) -> CommandResult;
}