use botx_api::bot::models::LeftFromChatCommand;

use crate::{contexts::RequestContext, results::CommandResult};

#[async_trait_with_sync::async_trait]
pub trait ILeftFromChatHandler: Send + Sync {
    async fn handle(&mut self, event: LeftFromChatCommand<serde_json::Value>, request_context: RequestContext) -> CommandResult;
}