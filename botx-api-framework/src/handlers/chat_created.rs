use botx_api::bot::models::ChatCreatedCommand;

use crate::{contexts::RequestContext, results::CommandResult};

#[async_trait_with_sync::async_trait]
pub trait IChatCreatedHandler: Send + Sync {
    async fn handle(&mut self, event: ChatCreatedCommand<serde_json::Value>, request_context: RequestContext)
        -> CommandResult;
}