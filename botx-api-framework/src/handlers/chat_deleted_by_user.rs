use botx_api::bot::models::ChatDeletedByUserCommand;

use crate::{contexts::RequestContext, results::CommandResult};

#[async_trait_with_sync::async_trait]
pub trait IChatDeletedByUserHandler: Send + Sync {
    async fn handle(&mut self, event: ChatDeletedByUserCommand<serde_json::Value>, request_context: RequestContext)
        -> CommandResult;
}
