use crate::{contexts::RequestContext, results::CommandResult};

#[async_trait_with_sync::async_trait]
pub trait IMessageHandler: Send + Sync {
    async fn handle(&mut self, message: String, request_context: RequestContext) -> CommandResult;
} 