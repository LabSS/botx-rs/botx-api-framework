use botx_api::bot::models::CtsLoginCommand;

use crate::{contexts::RequestContext, results::CommandResult};

#[async_trait_with_sync::async_trait]
pub trait ICtsLoginHandler: Send + Sync {
    async fn handle(&mut self, event: CtsLoginCommand<serde_json::Value>, request_context: RequestContext) -> CommandResult;
}