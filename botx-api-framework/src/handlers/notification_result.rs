use botx_api::{api::result::ExpressResult, bot::models::{NotificationCallbackRequestError, NotificationCallbackRequestOk}};

use crate::{contexts::RequestContext, results::CommandResult};

#[async_trait_with_sync::async_trait]
pub trait INotificationResultHandler: Send + Sync {
    async fn handle(&mut self, result: ExpressResult<NotificationCallbackRequestOk, NotificationCallbackRequestError>);
} 