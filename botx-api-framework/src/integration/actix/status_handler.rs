use actix_web::{web::{Data, self, Json}, get, Responder, HttpResponse, http::{header::ContentType, self}, error};
use anthill_di::DependencyContext;
use botx_api::{api::{result::ExpressResult, models::Status}, bot::models::*};

use crate::contexts::BotXApiFrameworkContext;

#[get("/status")]
pub async fn status(request: web::Query<StatusRequest>, ioc_ctx: Data<DependencyContext>) -> impl Responder {
    log::debug!("{:#?}", request);

    let context = ioc_ctx.resolve::<BotXApiFrameworkContext>().await.expect("Контекст фреймворка не может быть получен из ioc");

    Json(match context.process_status_result(request.0).await {
        Some(response) => response,
        None => StatusResponseBuilder::default().with_status(Status::Ok)
            .with_result(StatusResponseResultBuilder::default()
                .with_status_message("Бот активен")
                .with_enabled(true)
                .build()
                .unwrap())
            .build()
            .unwrap()
    })
}