use std::sync::Arc;

use actix_web::{web::{Data, self}, post, Responder, HttpResponse, http::{header::ContentType, self}, error};
use anthill_di::DependencyContext;
use async_lock::RwLock;
use botx_api::{api::result::ExpressResult, bot::models::{NotificationCallbackRequestOk, NotificationCallbackRequestError}};

use crate::contexts::{BotXApiFrameworkContext, NotificationContext};

#[post("/notification/callback")]
pub async fn notification_callback(request: web::Json<ExpressResult<NotificationCallbackRequestOk, NotificationCallbackRequestError>>, ioc_ctx: Data<DependencyContext>) -> impl Responder {
    if request.0.is_ok() {
        log::debug!("{:#?}", request);
    } else {
        log::error!("{:#?}", request);
    }

    let req = &request.0;

    let sync_id = match req {
        ExpressResult::<NotificationCallbackRequestOk, NotificationCallbackRequestError>::Ok(res) => res.sync_id.clone(),
        ExpressResult::<NotificationCallbackRequestOk, NotificationCallbackRequestError>::Err(err) => err.sync_id.clone(),
    };

    if let Ok(notification_context) = ioc_ctx.resolve::<Arc<RwLock<NotificationContext>>>().await {
        notification_context.write().await.event(sync_id);
    }

    let context = ioc_ctx.resolve::<BotXApiFrameworkContext>().await.expect("Контекст фреймворка не может быть получен из ioc");

    context.process_notification_result(request.0).await;

    HttpResponse::Ok()
}