use std::sync::Arc;

use anthill_di_derive::constructor;
use async_lock::RwLock;
use botx_api::api::{
    context::BotXApiContext,
    models::*,
    v4::notification::direct::{
        api::direct_notification,
        models::DirectNotificationRequestBuilder,
    }, utils::auth_retry::retry_with_auth,
};
use botx_api_framework::{
    button_data,
    contexts::RequestContext,
    handlers::button::IButtonHandler,
    results::{CommandOk, CommandResult},
};

use crate::handlers::button_first_handler::{FirstButtonHandlerData, FirstButtonHandlerMetaData};

#[derive(constructor)]
pub struct SecondButtonHandler {
    #[resolve]
    api: Arc<RwLock<BotXApiContext>>,
}

#[button_data]
pub struct SecondButtonHandlerData {
    pub test_data_payload: f64,
}

#[button_data]
pub struct SecondButtonHandlerMetaData {
    pub test_metadata_payload: u32,
}

#[async_trait_with_sync::async_trait]
impl IButtonHandler for SecondButtonHandler {
    type TData = SecondButtonHandlerData;
    type TMetaData = SecondButtonHandlerMetaData;

    async fn handle(
        &mut self,
        button_text: String,
        data: Self::TData,
        metadata: Self::TMetaData,
        request: RequestContext,
    ) -> CommandResult {
        let notification = EventPayloadBuilder::default()
            .with_body(button_text)
            .with_metadata(FirstButtonHandlerMetaData {
                type_id: Default::default(),
                test_metadata_payload: "test metadata".to_string(),
            })
            .with_keyboard(vec![vec![ButtonBuilder::default()
                .with_command("Команда от обработчика кнопок 2")
                .with_label(format!(
                    "Отправлено обработчиком 2, нажми меня! [{}] [{}]",
                    data.test_data_payload, metadata.test_metadata_payload
                ))
                .with_data(FirstButtonHandlerData {
                    type_id: Default::default(),
                    test_data_payload: "test data".to_string(),
                })
                .build()
                .unwrap()]])
            .build()
            .unwrap();

        let request = &DirectNotificationRequestBuilder::default()
            .with_group_chat_id(request.from.group_chat_id.unwrap())
            .with_notification(notification)
            .build()
            .unwrap();

        retry_with_auth(&self.api, || direct_notification(&self.api, request)).await.unwrap();

        Ok(CommandOk::default())
    }
}
