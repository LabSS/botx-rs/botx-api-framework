use std::sync::Arc;

use anthill_di_derive::constructor;
use async_lock::RwLock;
use botx_api::api::{
    context::BotXApiContext,
    models::*,
    v4::notification::direct::{
        api::direct_notification,
        models::DirectNotificationRequestBuilder,
    }, utils::auth_retry::retry_with_auth,
};
use botx_api_framework::{
    contexts::RequestContext,
    handlers::message::IMessageHandler,
    results::{CommandOk, CommandResult},
};

use crate::handlers::button_first_handler::{FirstButtonHandlerData, FirstButtonHandlerMetaData};

#[derive(constructor)]
pub struct MessageHandler {
    #[resolve]
    api: Arc<RwLock<BotXApiContext>>,
}

#[async_trait_with_sync::async_trait]
impl IMessageHandler for MessageHandler {
    async fn handle(&mut self, message: String, request: RequestContext) -> CommandResult {
        let notification = EventPayloadBuilder::default()
            .with_body(message)
            .with_metadata(FirstButtonHandlerMetaData {
                type_id: Default::default(),
                test_metadata_payload: "test metadata".to_string(),
            })
            .with_keyboard(vec![vec![ButtonBuilder::default()
                .with_command("Команда от обработчика сообщений")
                .with_label("Отправлено после сообщения, нажми меня")
                .with_data(FirstButtonHandlerData {
                    type_id: Default::default(),
                    test_data_payload: "test data".to_string(),
                })
                .build()
                .unwrap()]])
            .build()
            .unwrap();

        let request = &DirectNotificationRequestBuilder::default()
            .with_group_chat_id(request.from.group_chat_id.unwrap())
            .with_notification(notification)
            .build()
            .unwrap();

        retry_with_auth(&self.api, || direct_notification(&self.api, request)).await.unwrap();

        Ok(CommandOk::default())
    }
}
