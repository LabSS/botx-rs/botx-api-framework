use std::sync::Arc;

use anthill_di_derive::constructor;
use async_lock::RwLock;
use botx_api::api::{
    context::BotXApiContext,
    models::*,
    v4::notification::direct::{
        api::direct_notification,
        models::DirectNotificationRequestBuilder,
    }, utils::auth_retry::retry_with_auth,
};
use botx_api_framework::{
    button_data,
    contexts::RequestContext,
    handlers::button::IButtonHandler,
    results::{CommandOk, CommandResult},
};

use crate::handlers::button_second_handler::{
    SecondButtonHandlerData, SecondButtonHandlerMetaData,
};

#[derive(constructor)]
pub struct FirstButtonHandler {
    #[resolve]
    api: Arc<RwLock<BotXApiContext>>,
}

#[button_data]
pub struct FirstButtonHandlerData {
    pub test_data_payload: String,
}

#[button_data]
pub struct FirstButtonHandlerMetaData {
    pub test_metadata_payload: String,
}

#[async_trait_with_sync::async_trait]
impl IButtonHandler for FirstButtonHandler {
    type TData = FirstButtonHandlerData;
    type TMetaData = FirstButtonHandlerMetaData;

    async fn handle(
        &mut self,
        button_text: String,
        data: Self::TData,
        metadata: Self::TMetaData,
        request: RequestContext,
    ) -> CommandResult {
        let notification = EventPayloadBuilder::default()
            .with_body(button_text)
            .with_metadata(SecondButtonHandlerMetaData {
                type_id: Default::default(),
                test_metadata_payload: 5,
            })
            .with_keyboard(vec![vec![ButtonBuilder::default()
                .with_command("Команда от обработчика кнопок 1")
                .with_label(format!(
                    "Отправлено обработчиком 1, нажми меня! [{}] [{}]",
                    data.test_data_payload, metadata.test_metadata_payload
                ))
                .with_data(SecondButtonHandlerData {
                    type_id: Default::default(),
                    test_data_payload: 3.2,
                })
                .build()
                .unwrap()]])
            .build()
            .unwrap();

        let request = &DirectNotificationRequestBuilder::default()
            .with_group_chat_id(request.from.group_chat_id.unwrap())
            .with_notification(notification)
            .build()
            .unwrap();

        retry_with_auth(&self.api, || direct_notification(&self.api, request)).await.unwrap();

        Ok(CommandOk::default())
    }
}
